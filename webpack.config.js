const path = require('path');
// const TerserPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const webpack = require('webpack');

 
module.exports = {
    mode: 'development',
    entry: './src/index.js',
    output: {
        // filename: 'bundle.js',
        filename: '[name].[contenthash].js', // for browser caching
        path: path.resolve(__dirname, 'dist'),
        // publicPath: 'dist/' // tells webpack where all the generated files are located
        // publicPath: '' // caching 
    },
    module: {
        rules: [
            {
                test: /\.(png|jpg)$/,
                use: ['file-loader']
            },
            {
                test: /\.(c|sc)ss$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']//MiniCssExtractPlugin.loader
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/env'],
                        plugins: ['transform-class-properties']
                    }
                }
            },
        ]
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: 'index.html'
        }),
        new BundleAnalyzerPlugin(),
        new webpack.ProvidePlugin({
            _: 'lodash',
        }),
        new MiniCssExtractPlugin({
            filename: 'styles.css'
            // filename: 'styles.[hash].css' // caching
        }),

    ],
    optimization: {
        runtimeChunk: true,
        splitChunks: {
            cacheGroups: {
                vendor: {
                    test: /[\\/]node_modules[\\/](react|react-dom)[\\/]/,
                    name: 'nodemodules',
                    chunks: 'all',
                },
                vendor1: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'loadash',
                    chunks: 'all',
                },
            },
        },
    }
}