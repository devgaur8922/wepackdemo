import './button.scss';

export default class Button {
    buttonClassName = 'button';
    render() {
        const button = document.createElement('button');
        // button.innerHTML = 'Sample!!@@@@!';
        button.innerHTML = _.join(['Hello', 'webpack'], ' ');
        button.classList.add('button');
        button.classList.add(this.buttonClassName);
        button.onclick = () => {
            console.log('Button is clicked!!');
        }
        document.body.appendChild(button);
    }
}